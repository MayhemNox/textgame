import random
import time


class SurvivalOfTheFittest:
    def __init__(self):
        self.title = "Welcome to the RPG game: 'Only the luckiest survive!'"
        print(self.title)
        self.ask_for_choice = "What will you do?\n" * 1
########################################################################################################################
        self.first_room = {
             '1': "\n\nThere is a blue dwarf waiting for visitors.\n"
                  "He doesn't look like much.",
             '2': "\n\nThere is a human holding a big poleaxe and he is BIG.\n",
             '3': "\n\nThere is an undead skeleton with a hoodie on and there is burnt ground around him.\n"
                  "Looks like a badass if you ask me, be careful!\n"
         }
        self.first_encounter = {
            '1': '\nA wild boar appears, he has ridiculous big and pointy tusks. You should be careful around him.\n',
            '2': '\nA pack of thieves appears, they were the most renown thieves in the county.\n'
                 'The rumours are that they once killed 4 knights protecting a powerful lord named Zucchilian,'
                 'and then proceeding with robbing him.\n',
            '3': '\nYou see a camp, the fire is still burning. Something is odd you say.\n'
                 'There is no one there and there are 4 types of food on the ground. You are really hungry.\n'
                 'You dare to take one type of food from the ground and eat it.'
                 ' Be careful, only 1 is good, and one will kill you. Good luck! You will need it.\n'
        }

        self.hp = {
            "Warrior": 110,
            "Mage": 90,
            "Rogue": 100,
            "Warlock": 95
        }
########################################################################################################################
        self.pick_a_name = input("Please name your brave champion: ").capitalize()
        self.pick_a_class = input("\nPlease write down the class you would like to play."
                                  "\nWarrior (has increased hp) "
                                  "\nMage (deals increased damage to humanoids "
                                  "\nRogue (has the ability to vanish into the shadows) "
                                  "\nWarlock (deals increased damage to undeads)\n ").strip().capitalize()
        self.classPick()
        time.sleep(3)

        self.first_room_generator()
        self.first_encounter_generator()

    def classPick(self):
        class_list = ["Warrior", "Mage", "Rogue", "Warlock"]
        while self.pick_a_class in str(class_list):
            return self.pick_class_name()
            break
        else:
            print("Please write a class from the listed characters.\n"
                  "The game will re-run\n")
            self.__init__()

    def pick_class_name(self):
        if self.pick_a_class == 'Warrior':
            print("\nYou are a brave Warrior named {} and you got {} hp.".format(self.pick_a_name, self.hp["Warrior"]))
        elif self.pick_a_class == 'Mage':
            print("\nYou are a brave Mage named {} and you got {} hp.".format(self.pick_a_name, self.hp["Mage"]))
        elif self.pick_a_class == 'Rogue':
            print("\nYou are a brave Rogue named {} and you got {} hp.".format(self.pick_a_name, self.hp["Rogue"]))
        elif self.pick_a_class == 'Warlock':
            print("\nYou are a brave Warlock named {} and you got {} hp.".format(self.pick_a_name, self.hp["Warlock"]))
        else:
            print("\nPlease select from the presented characters.\n")

    def first_room_generator(self):
        print("\n\nAfter walking through the snow for a couple of hours, you finally reach "
              "your first chamber.\nBy the way, your main goal is to reach to the end safe.")
        time.sleep(5)
        self.random_chamber = random.choice(list(self.first_room.values()))
        print(self.random_chamber)
        print(self.ask_for_choice)

        if self.random_chamber == self.first_room["1"]:
            time.sleep(2)
            self.pick_a_choice = input("\n1) Kill him\n2) Say hi\n3) Mind your own business, move along"
                                       "\n4) Pay him some gold\n")
            if self.pick_a_choice == '1':
                print("\nYou successfully kill him by attacking first and taking him by surprise."
                      "\nHe couldn't stand a chance")
                time.sleep(2)
            elif self.pick_a_choice == '2':
                print("Nothing happens, you continue your journey safe (for now).")
                time.sleep(2)
            elif self.pick_a_choice == '3':
                self.hp[self.pick_a_class] = self.hp[self.pick_a_class] - 10
                print("He says hi, you ignore him."
                      "Watching towards him distracts your attention and you trip and fell."
                      " You now have {} hp.".format(self.hp[self.pick_a_class]))

            elif self.pick_a_choice == '4':
                print("The dwarf tells you a joke for being so kind, he says:\n"
                      "'How many Dwarves does it take to change a light bulb? "
                      "Three. One to hold the bulb, and two to drink until the room starts to spin.'")
            else:
                print("Please select one of the four options.\nA new random room will be generated.")
                return self.first_room_generator()

        elif self.random_chamber == self.first_room["2"]:
            time.sleep(2)
            self.pick_a_choice = input("1) Kill him\n2) Say hi\n3) Mind your own business, move along"
                                       "\n4) Pay him some gold\n")
            if self.pick_a_choice == '1' and self.pick_a_class in ('Warrior', 'Rogue'):
                print("He crushes your skull, you cannot kill him with a melee character. HA")
                print("You DIEDED")
                raw = input("Do you want to continue? y/n")
                if raw == 'y':
                    return SurvivalOfTheFittest()
                else:
                    quit()
            elif self.pick_a_choice == '1' and self.pick_a_class not in ('Warrior', 'Rogue'):
                print("You casted one of your greatest spell and kill him almost in an instant. Good job")

            elif self.pick_a_choice == '2':
                print("He is coming aggressively towards you, so you attack first and strike him down. BAM! He's dead")
            elif self.pick_a_choice == '3':
                print("Nothing happens, he does not care. You continue your journey.")
            elif self.pick_a_choice == '4':
                self.hp[self.pick_a_class] = self.hp[self.pick_a_class] + 5
                print("You receive a mini-health potion, increase your hp by 5.\n"
                      "You now have {} hp".format(self.hp[self.pick_a_class]))
            else:
                print("Please select one of the four options.\nA new random room will be generated.")
                return self.first_room_generator()

        elif self.random_chamber == self.first_room["3"]:
            time.sleep(2)
            self.pick_a_choice = input("1) Kill him\n2) Say hi\n3) Mind your own business, move along"
                                       "\n4) Pay him some gold\n")
            if self.pick_a_choice == '1' and self.pick_a_class == 'Mage':
                print("You cast a polymorph spell on him and move along")
            elif self.pick_a_choice == '2' and self.pick_a_class == 'Warlock':
                print("You get more powerful spells against undeads so this gonna be easy for you.\n"
                      "Cast a ShadowFlame bolt and the skeleton is gonna tranform into a pile of bones.")
            elif self.pick_a_choice == '3':
                print("You can easily outrun him, he's just a skeleton afterall")
            elif self.pick_a_choice == '4':
                print("Undeads don't use gold, what is wrong with you?\n"
                      "He takes this as an insult and throws a bone towards you, you lose 5 hp and "
                      "move towards the next encounter.")
                print("You now have {} hp.".format(self.hp[self.pick_a_class]-5))
            else:
                print("You have to choose from the displayed options.\n"
                      "Please try again.")
                return self.first_room_generator()

    def first_encounter_generator(self):
        time.sleep(3)
        print("\nCongrats for getting out alive from the first room."
              "Now that you have passed the first test, you move on towards the next one.\n"
              "You left the city and now you have reached the woods. On your way to the second room,"
              "You hear some noises in the bushes.")
        time.sleep(5)
        self.random_encounter = random.choice(list(self.first_encounter.values()))
        print(self.random_encounter)
        print(self.ask_for_choice)

        if self.random_encounter == self.first_encounter['1']:
            time.sleep(2)
            self.pick_a_choice = input("1) ")




    def second_room_generator(self):
        pass


if __name__ == '__main__':
    SurvivalOfTheFittest()
